#!/bin/env python3
"""Receipt class.

Author Vinícius Gabriel Linden
Date 2023-05-18
"""

from __future__ import annotations
from .money import Money


class Receipt(dict[str, Money]):
    """Receipt.

    This forces the user to use a dictionary whose keys are always strings and
    values are always Money
    """

    def __init__(self, *args) -> None:
        super().__init__(*args)
        for key, value in self.items():
            if not isinstance(key, str):
                raise TypeError("Key must be a string")
            if not isinstance(value, Money):
                raise TypeError("Value must be a Money object")

    def __setitem__(self, key, value) -> None:
        if not isinstance(key, str):
            raise TypeError("Key must be a string")
        if not isinstance(value, Money):
            raise TypeError("Value must be a Money object")
        return super().__setitem__(key, value)

    def total(self) -> Money:
        """Get total receipt value."""
        return Money(sum(self.values()))

    def __repr__(self) -> str:
        # TODO use prettytable
        if len(self) == 0:
            return "EMPTY RECEIPT"
        columns = [0, 0]
        for name, value in self.items():
            length = len(str(name))
            if length > columns[0]:
                columns[0] = length + 1
            length = len(str(value))
            if length > columns[1]:
                columns[1] = length
        ret = ""
        for name, value in self.items():
            ret += "{0:<{left}} {1:>{right}}\n".format(name + ":", value, left=columns[0], right=columns[1])
        return ret[:-1]
