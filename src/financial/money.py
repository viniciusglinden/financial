from copy import deepcopy
from math import isclose
from typing import Self

from .currency import Currency


class Money:
    value: float
    currency: str

    def __init__(self, amount: Self | float | int = 0.0, currency: str = "EUR") -> None:
        """Constructor.

        :param amount:
            Amount of money.
        :param currency:
            Used currency.
        """

        if isinstance(amount, type(self)):
            self.value = amount.value
            self.currency = amount.currency
            if self.currency != currency:
                self.value *= self.conversion_value(currency)
                self.currency = currency
        else:
            self.value = float(amount)
            self.currency = str(currency)
        Currency().symbol_get(self.currency)

    def conversion_value(self, to_currency: str = "BRL", force_refresh: bool = False) -> float:
        """Get conversion value from current currency to a target currency.

        :param to_currency:
            Target currency.
        :param force_refresh:
            Download database file again.
        :return:
            Conversion value.
        """
        Currency().symbol_get(to_currency)
        if self.currency == to_currency:
            return 1.0
        if force_refresh:
            Currency().json_delete()
        Currency().json_download()

        return Currency().conversion_get(self.currency, to_currency)

    def convert(self, to_currency: str = "BRL") -> Self:
        """Convert current object from one currency to another.

        :param to_currency:
            Target currency.
        :return:
            A copy of current object, in the target currency.
        """
        return self.__class__(self.value * self.conversion_value(to_currency), to_currency)

    def copy(self) -> Self:
        """Copy this object."""
        return deepcopy(self)

    def __add__(self, rhs) -> Self:
        """Add number to value.

        :param rhs:
            Value or money to be added.
        :return:
            Summed money.
        """
        rhs = self.__class__(rhs, self.currency)
        return self.__class__(self.value + rhs.value, self.currency)

    def __radd__(self, rhs) -> Self:
        """Reverse sum, used in sum().

        :param rhs:
            Value or money to be added.
        :return:
            Summed money.
        """
        rhs = self.__class__(rhs)
        # TODO test if it is rhs + self and use rhs currency if so
        return self.__add__(rhs)

    def __sub__(self, rhs: Self | float | int) -> Self:
        """Subtract number to value.

        :param rhs:
            Value or money to be subtracted.
        :return:
            Subtracted money.
        """
        if not isinstance(rhs, type(self)):
            rhs = self.__class__(rhs, self.currency)
        return self.__class__(self.value - rhs.value, self.currency)

    def __mul__(self, rhs) -> Self:
        """Multiplication method.

        :param rhs:
            Value to be multiplied.
        :return:
            Multiplied money.
        """
        if isinstance(rhs, type(self)):
            raise TypeError("Must not be a of Money type")
        return self.__class__(self.value * rhs, self.currency)

    def __rmul__(self, rhs) -> Self:
        """Reverse multiplication method.

        :param rhs:
            Value to be multiplied.
        :return:
            Multiplied money.
        """
        return self.__mul__(rhs)

    def __truediv__(self, rhs) -> Self | float:
        """True divide method.

        :param rhs:
            Value to be divided.
        :return:
            Divided money.
        """
        if isinstance(rhs, type(self)):
            return self.conversion_value(rhs.currency)
        return self.__class__(self.value / rhs, self.currency)

    def __eq__(self, rhs) -> bool:
        """Equality operator.

        :param rhs:
            Value or money to be compared.
        """
        rhs = self.__class__(rhs, self.currency)
        tolerance = Currency().tolerance_get(self.currency)
        return isclose(self.value, rhs.value, abs_tol=tolerance)

    def __neq__(self, rhs) -> bool:
        """Inequality operator.

        :param rhs:
            Value or money to be compared.
        """
        return not self.__eq__(rhs)

    def __lt__(self, rhs) -> bool:
        """Less than operator.

        :param rhs:
            Value or money to be compared.
        """
        rhs = self.__class__(rhs, self.currency)
        return self.value < rhs.value

    def __le__(self, rhs) -> bool:
        """Less than or equal to operator.

        :param rhs:
            Value or money to be compared.
        """
        rhs = self.__class__(rhs, self.currency)
        return self.__lt__(rhs) or self.__eq__(rhs)

    def __gt__(self, rhs) -> bool:
        """Greater than operator.

        :param rhs:
            Value or money to be compared.
        """
        rhs = self.__class__(rhs, self.currency)
        return self.value > rhs.value

    def __ge__(self, rhs) -> bool:
        """Greater than or equal to operator.

        :param rhs:
            Value or money to be compared.
        """
        rhs = self.__class__(rhs, self.currency)
        return self.__gt__(rhs) or self.__eq__(rhs)

    def __repr__(self) -> str:
        """String representing the money.

        :return:
            String, for example € 10.00
        """
        return f"{Currency().symbol_get(self.currency)} {self.value:0.2f}"

    def __format__(self, format_spec: str) -> str:
        """Used in string formatting"""
        return format(str(self), format_spec)

    def __hash__(self) -> int:
        return hash(f"{self.value}{self.currency}")
