#!/usr/bin/env python3
"""Salary base class.

Author Vinícius Gabriel Linden
Date 2022-02-07
"""

from __future__ import annotations
from abc import ABC, abstractmethod
from copy import deepcopy
from typing import Dict, Tuple, Union

from .money import Money
from .receipt import Receipt

sanitize = lambda x, min=0.0, max=1.0: x < min or x > max


class Salary(ABC):
    _brutto: Money
    _extra: Receipt = Receipt()
    _tax: Receipt = Receipt()
    _costs: Receipt = Receipt()
    _donations: float
    _keep: float
    _currency = "USD"

    def brutto_get(self) -> Money:
        """Get original brutto value."""
        return self._brutto

    @abstractmethod
    def donations_get(self, **kwargs) -> Money:
        pass

    def donations(self, percentage: float):
        """Set donations percentage

        :param percentage:
            a float between 0.0 and 1.0.
        """
        sanitize(percentage)
        self._donations = float(percentage)

    def keep_get(self) -> Money:
        """Get calculated keep value."""
        return self._keep * self._brutto

    def keep(self, percentage: float) -> None:
        """Set Keep value.

        :param percentage:
            a float between 0.0 and 1.0.
        """
        sanitize(percentage)
        self._keep = float(percentage)

    @abstractmethod
    def netto_get(self, *args, **kwargs) -> Money:
        pass

    def _setter(self, what: str, **kwargs: Receipt) -> None:
        """Internal method to set a Salary dictionary.

        :param kwargs:
            A receipt to be set.
        """
        setattr(self, what, Receipt())  # delete previous values
        temp = Receipt()
        for name, value in kwargs.items():
            temp[str(name)] = Money(value)  # pyright: ignore
        setattr(self, what, temp)

    def extra(self, **earnings) -> Salary:
        """Set extra earnings.

        :param earnings:
            Any named earning, listed as a Receipt.
        :return:
            self
        """
        self._setter("_extra", **earnings)
        return self

    def extra_get(self) -> Money:
        """Get total extra earnings.

        :return:
            Total earnings, in Money.
        """
        return self._extra.total()

    def tax_get(self) -> Money:
        """Get total tax.

        :return:
            Total monthly tax, in Money.
        """
        return self._tax.total()

    def tax(self, **taxes) -> None:
        """Set taxes.

        :param taxes:
            Any named tax, listed as a Receipt.
        """
        self._setter("_tax", **taxes)

    def costs(self, **costs) -> None:
        """Set living costs.

        :param costs:
            Any named living costs, listed as a Receipt.
        """
        self._setter("_costs", **costs)

    def costs_get(self) -> Money:
        """Get total living costs.

        :return:
            Total monthly costs, in Money.
        """
        return self._costs.total()

    def check(self) -> bool:
        """Check if there is still money left in the end of the month.

        :return:
            True if there is still money left.
        """
        taxes = self._tax.total()
        costs = self._costs.total()
        donations = self.donations_get()
        extras = self._extra.total()
        netto = self._brutto + extras - taxes - donations - costs
        return netto > 0

    def copy(self) -> Salary:
        """Create a copy of current object."""
        return deepcopy(self)

    def __repr__(self) -> str:
        ret = ""
        receipt = Receipt()
        receipt["Brutto"] = self._brutto
        receipt["Donations"] = self.donations_get()
        receipt.update(self._extra)
        if self._costs:
            receipt.update(self._costs)
            costs = self._costs.total()
            costs_percentage = (costs / self._brutto) * 100
            ret = f"\nTaxes: {costs_percentage:.2f} %"
        if self._tax:
            receipt.update(self._tax)
            tax_percentage = (self._tax.total() / self._brutto) * 100
            ret = f"\nTaxes: {tax_percentage:.2f} %"
        receipt["Keep"] = self.keep_get()
        receipt["Netto"] = self.netto_get(True)

        ret = str(receipt) + ret
        donations = self._donations * 100
        ret += f"\nDonations: {donations:.2f} %"
        keep = self._keep * 100
        ret += f"\nKeep: {keep:.2f} %"
        return ret.replace("_", " ")
