from __future__ import annotations
from typing import Tuple, Union, List
from sys import exit as sysexit

from .money import Money
from .salary import Salary, sanitize
from .receipt import Receipt


class German(Salary):
    """Monthly calculations for German salary."""

    def __init__(self, brutto: Money, keep: float, donations: float, work_hours=160) -> None:
        """Create new paycheck track

        :param brutto:
            Brutto salary.
        :param keep:
            Planned amount of money to keep.
        :param donations:
            Planned amount of money to donate.
        :param work_hours:
            Weekly work hours.
        """
        if sanitize(work_hours, 0, 12 * 4 * 7):
            raise ValueError("Invalid parameter")
        if sanitize(keep) or sanitize(donations):
            raise ValueError("Invalid parameter")

        self._brutto: Money = Money(brutto)  # type: ignore
        self._keep: float = float(keep)
        self._donations: float = float(donations)
        self._currency = "EUR"

    def netto_get(self, with_kirchensteuer: bool = True) -> Money:
        """Get calculated netto.

        :param with_kirchensteuer:
            Deduct Kirchensteuer from the netto, like in the end of monthly
            report.
        :return:
            Netto.
        """
        taxes = self._tax
        if not with_kirchensteuer:
            taxes.pop("Kirchensteuer")
        return self._brutto - self._tax.total()

    def tax(
        self,
        Lohnsteuer: Money,
        Kirchensteuer: Money,
        KV: Money,
        RV: Money,
        AV: Money,
        PV: Money,
    ) -> Salary:
        """Set taxes."""
        return super().tax(
            Lohnsteuer=Lohnsteuer,
            Kirchensteuer=Kirchensteuer,
            KV=KV,
            RV=RV,
            AV=AV,
            PV=PV,
        )

    def donations_get(self, with_kirchensteuer: bool = True) -> Money:
        """Get donation value

        :param with_kirchensteuer:
            Keep Kirchensteuer as a donation.
        :return:
            Set donation index and money.
        """
        money = self._brutto.copy() + self.extra_get()
        money = money * self._donations
        if not with_kirchensteuer:
            money -= self._tax["Kirchensteuer"]
        return money
