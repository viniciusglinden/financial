from json import load as json_load
from pathlib import Path
import tempfile
from typing import Final, cast, TypedDict

from requests import get


JSON_ROOT_PATH: Final[Path] = Path(tempfile.mkdtemp())


class CurrenciesEntry(TypedDict):
    symbol: str
    is_fiat: bool
    tolerance: float


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Currency(metaclass=SingletonMeta):
    """Keep and manage currencies and JSON files for conversion.

    This class implements lazy methods for download.
    """

    _currencies: dict[str, CurrenciesEntry] = {
        "BRL": {"symbol": "R$", "is_fiat": True, "tolerance": 0},
        "BTC": {"symbol": "₿", "is_fiat": False, "tolerance": 0},
        "EUR": {"symbol": "€", "is_fiat": True, "tolerance": 0},
        "USD": {"symbol": "$", "is_fiat": True, "tolerance": 0},
        "XMR": {"symbol": "XMR", "is_fiat": False, "tolerance": 0},
    }

    def __init__(self, tolerance: float = 0.05) -> None:
        """Create a singleton class.

        Parameters can only be set upon creation.

        :param tolerance:
            Absolute tolerance in € to consider when comparing two currencies too lower
            values may cause false results.
        """
        JSON_ROOT_PATH.mkdir(parents=True, exist_ok=True)

        self._currencies["EUR"]["tolerance"] = tolerance

    def json_download(self, currency: str = "") -> None:
        """Download the current conversion files and write tolerance.

        This makes it more stable and spares download time.
        This method will not download if file already exists.

        :param currency:
            Currency to download.
        """
        currency = currency.upper()
        currencies = {currency} if currency else self.currencies_get()

        for currency in currencies:
            file = JSON_ROOT_PATH / f"{currency}.json"
            if not file.is_file():
                url = (
                    r"https://min-api.cryptocompare.com/data/price?fsym="
                    + currency
                    + r"&tsyms="
                    + ",".join(self._currencies.keys())
                )
                r = get(url, timeout=2)
                file.open("wb").write(r.content)

    def json_delete(self, currency: str = "") -> None:
        """Deletes all JSON files.

        :param currency:
            Currency to delete.
        """
        currency = currency.upper()
        currencies = {currency} if currency else self.currencies_get()

        for currency in currencies:
            file_path = JSON_ROOT_PATH / f"{currency}.json"
            file_path.unlink(missing_ok=True)

    def currencies_get(self) -> set[str]:
        """Return available currency names."""
        return set(self._currencies.keys())

    def symbol_get(self, currency: str) -> str:
        """Return currency symbol."""
        return self._currencies[currency.upper()]["symbol"]

    def tolerance_get(self, currency: str = "brl", tolerance: float = 0.0) -> float:
        """This gets the absolute tolerance for currency.

        Why the conversion? Because rounding different values will result in
        inconsistencies.
        Eg. rounding €0.009 to €0.01 is not the same as rounding ₿0.009 to ₿0.01.

        :param currency:
            Currency to calculate the tolerance.
        :param tolerance:
            Absolute tolerance value in €, if != 0, recalculate everything again.
        :return:
            Absolute tolerance for the currency.
        """
        currency = currency.upper()
        self._currencies[currency]

        if tolerance != 0.0:
            for value in self._currencies.values():
                if value["symbol"] == "€":
                    value["tolerance"] = tolerance
                else:
                    value["tolerance"] = 0.0

        if self._currencies[currency]["tolerance"] == 0.0:
            conversion = self.conversion_get("EUR", currency)
            tolerance = conversion * self._currencies["EUR"]["tolerance"]
            self._currencies[currency]["tolerance"] = tolerance

        return self._currencies[currency]["tolerance"]

    def is_fiat(self, currency: str) -> bool:
        """Returns if currency is fiat.

        :param currency:
            Currency to test.
        :return:
            If currency is fiat.
            True if currency is fiat
        """
        currency = currency.upper()
        return self._currencies[currency]["is_fiat"]

    def conversion_get(self, from_currency: str = "EUR", to_currency: str = "BRL") -> float:
        """Get conversion value.

        :param str from_currency:
            Currency to convert from.
        :param str to_currency:
            Currency to convert to.
        :return:
            Conversion value.
        :rtype:
            float.
        """
        from_currency = from_currency.upper()
        to_currency = to_currency.upper()
        self._currencies[from_currency]
        self._currencies[to_currency]
        self.json_download(from_currency)
        file = JSON_ROOT_PATH / f"{from_currency}.json"
        with file.open("r") as fp:
            return cast(float, json_load(fp)[to_currency])
