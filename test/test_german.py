from unittest import TestCase, main

from financial.german import German
from financial.money import Money


class Test_german(TestCase):
    def test_default_currency(self):
        salary = German(1, 0, 0)
        self.assertEqual(salary._currency, "EUR")

    def test_extra(self):
        salary = German(1, 0, 0)
        salary.extra(a=1, b=2)
        self.assertEqual(salary.extra_get(), 3)

    def test_normal_usage(self):
        brutto = Money(123)
        salary = German(brutto, 0.3, 0.3, 159)
        salary.tax(1, 2, 3, 4, 5, 6)
        salary.costs(car=1, housing=2)
        salary.extra(bonus=3, end_of_year=5)
        taxes = Money(sum(salary._tax.values()))
        self.assertEqual(taxes, salary.tax_get())
        costs = Money(sum(salary._costs.values()))
        self.assertEqual(costs, salary.costs_get())
        extras = Money(sum(salary._extra.values()))
        self.assertEqual(extras, salary.extra_get())

        netto = brutto - taxes
        self.assertEqual(salary.netto_get(), netto)
        self.assertTrue(salary.check())
        self.assertEqual(taxes.currency.upper(), "EUR")
        self.assertEqual(costs.currency.upper(), "EUR")
        self.assertEqual(extras.currency.upper(), "EUR")
