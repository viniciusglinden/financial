from unittest import TestCase, main
from json import load as json_load

from financial.currency import Currency
from financial.money import Money
import pytest


def test_money_create() -> None:
    Money(0, "bRl")
    with pytest.raises(Exception):
        Money(1, "lol")
    Money(Money(1))


def test_money_print() -> None:
    a = str(Money(987654321.123456789))
    assert a == "€ 987654321.12"
    a = str(Money(10, "BRL"))
    assert a == "R$ 10.00"


# def test_conversion() -> None:
#     for i in range(0, 5):  # repeat this test so that equality is certain to not fail
#         Currency().json_download("eur")
#
#         file = open("/tmp/financial-EUR.json")
#         eur2brl = json_load(file)["BRL"]
#         file.close()
#         assert (Currency().conversion_get() == eur2brl)
#         a = Money(12.12, "EUR")
#         b = Money(12.12 * eur2brl, "BRL")
#         assert (a == b)
#         assert (a >= b)
#         assert (a <= b)
#         c = Money(a, "BRL")  #
#         assert (b == c)  #
#         assert (b >= c)  #
#         assert (b <= c)  #


def test_comparison() -> None:
    a = Money(100)
    assert a > 1
    assert a >= 100
    assert not (a >= 200)
    assert not (a < 1)
    assert not (a <= 1)
    assert a <= 100

    b = Money(100)
    assert a == b
    assert not (a != b)
    assert a >= b
    assert a <= b
    assert not (a > b)
    assert not (a < b)

    b = Money(110.00)
    assert not (a == b)
    assert a != b
    assert not (a >= b)
    assert a <= b
    assert not (a > b)
    assert a < b

    b += 10
    assert b == 120
    b -= 10
    assert b == 110
    b += Money(10)
    assert b == 120
    b -= Money(10)
    assert b == 110


def test_money_operations() -> None:
    a = 0 + Money(1)
    assert str(a) == "€ 1.00"

    sigma = sum([Money(1) + Money(2) + Money(3)])
    assert sigma == sum([1 + 2 + 3])
    sigma = sum([Money(1) + Money(2, "brl")])
    assert sigma.currency == "EUR", "currency should be inherited from the first element"

    a = Money(2) * 3
    assert a.value == 6
    a = 2 * Money(2)
    assert a.value == 4
    a = Money(1) / 2
    assert a.value == 0.5

    eur = Money(1, "eur")
    brl = Money(1, "brl")
    eur2brl = eur / brl
    assert eur2brl == pytest.approx(Currency().conversion_get(), 0.05)


def test_hash() -> None:
    # should not trow an exception
    a = Money(1)
    hash(a)
