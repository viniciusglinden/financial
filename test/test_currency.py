from collections.abc import Generator
import pytest
from os.path import isfile

from financial.currency import Currency


@pytest.fixture
def currency_setup() -> Generator[None, None, None]:
    Currency()
    yield None
    Currency().json_delete()


def test_currency_download(currency_setup) -> None:
    Currency().json_download()
    Currency().json_delete()  # testing if an exception is raised (should not)

    Currency().json_download()
    Currency().json_delete("BrL")

    # This does not throw exception
    Currency().json_delete("lol")

    Currency().json_delete()
    Currency().json_download("bRl")


def test_currency_conversion(currency_setup) -> None:
    eur2brl = Currency().conversion_get()
    brl2eur = Currency().conversion_get("brl", "eur")
    assert eur2brl == pytest.approx(1 / brl2eur, 0.02)


def test_currency_crypto_fiat(currency_setup) -> None:
    a = Currency()
    assert a.is_fiat("brl")
    assert not a.is_fiat("btc")


def test_currency_symbols(currency_setup) -> None:
    assert Currency().symbol_get("brl") == "R$"
