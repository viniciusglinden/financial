
# Bookkeeping library

[Gitlab](https://gitlab.com/viniciusglinden/financial)

## Running unit tests

```sh
uv run pytest
```

## Building documentation

```sh
uv run sphinx-autobuild docs/source docs/build
```
