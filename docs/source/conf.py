# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "financial"
copyright = "2025, Vinícius Gabriel Linden"
author = "Vinicius Gabriel Linden"
PROJECT_ROOT = "../.."

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["autodoc2", "myst_parser", "sphinx_rtd_dark_mode"]

templates_path = ["_templates"]
exclude_patterns = []
source_suffix = {".md": "markdown"}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "alabaster"
html_static_path = ["_static"]
html_context = {"default_mode": "dark"}

# https://myst-parser.readthedocs.io/en/latest/configuration.html
# myst_enable_extensions = ["toctree"]

# https://sphinx-autodoc2.readthedocs.io/en/latest/config.html
autodoc2_render_plugin = "myst"
autodoc2_packages = [
    {"path": f"{PROJECT_ROOT}/src/financial"},
    {"path": f"{PROJECT_ROOT}/test"},
]
